﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Sirenix.OdinInspector;
using UnityEngine;

public class LobbiesManager : MonoBehaviour
{
    #region Fields

    [Serializable]
    public struct Lobby
    {
        public GameObject lobbyPanel;
        public GamesRoomControl.GameOptions.Games game;
        public List<Sprite> characters;
        public bool asToSetCharRandomly;
    }
    
    public List<Lobby> lobbies = new List<Lobby>();

    private GamesRoomControl gamesRoomControl;
    private MultiplayerLauncher multiplayerLauncher;
    private List<GameObject> playerLobbyLocations = new List<GameObject>();
    private GameObject correctLobby;
    
    #endregion
    
    #region UnityCallbacks

    private void Awake()
    {
        GameObject multiplayerController = FindObjectOfType<MultiplayerLauncher>().gameObject;
        if (gamesRoomControl == null)
            gamesRoomControl = multiplayerController.GetComponent<GamesRoomControl>();
        if (multiplayerLauncher == null)
        {
            multiplayerLauncher = multiplayerController.GetComponent<MultiplayerLauncher>();
            multiplayerLauncher.OnRoomJoined += SetActiveLobbyPanel;
        }
    }

    #endregion

    #region Methods

    public void SetActiveLobbyPanel()
    {
        correctLobby = lobbies.Find(lobby => lobby.game == gamesRoomControl.chosenGame.game).lobbyPanel;
        correctLobby.SetActive(true);
        
        GetPlayersLocations(correctLobby);
    }

    private void GetPlayersLocations(GameObject currentLobby)
    {
        playerLobbyLocations.Clear();
        
        foreach (Transform child in currentLobby.transform)
        {
            if (child.name.Contains("Player"))
                playerLobbyLocations.Add(child.gameObject);
        }
    }
    
    

    #endregion
}
