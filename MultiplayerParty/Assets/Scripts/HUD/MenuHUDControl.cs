﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MenuHUDControl : MonoBehaviour, IPointerClickHandler
{
    #region Fields

    [Title("Panels")]
    public List<GameObject> mainPanels = new List<GameObject>();
    public GameObject loadingPanel;
    public GameObject playerInfoPanel;

    [Title("Miscellaneous")] 
    public GameObject gameTitle;
    public Color playerColor;

    [HideInInspector]
    public MultiplayerLauncher multiplayerLauncher;
    
    private GameObject currentPanel;

    #endregion

    #region UnityCallbacks

    private void Awake()
    {
        multiplayerLauncher = FindObjectOfType<MultiplayerLauncher>();
        multiplayerLauncher.OnGameStarted += () => SetActiveNextPanel(loadingPanel);
        multiplayerLauncher.OnGameLoaded += () => SetActiveNextPanel(mainPanels[0]);
        multiplayerLauncher.OnGameLoaded += () => playerInfoPanel.SetActive(true);
        multiplayerLauncher.OnRoomJoined += () => SetActiveNextPanel(mainPanels[2]);

        InitHUD();
    }
    
    public void OnPointerClick(PointerEventData eventData)
    {
        Debug.Log("On Pointer Click");
        eventData.position = Input.mousePosition;
        
        List<RaycastResult> raycastResults = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventData, raycastResults);

        for (int i = 0; i < raycastResults.Count; i++)
        {
            if (raycastResults[i].gameObject.GetComponent<ColorGradient>() != null)
            {
                GetColorFromGradient(raycastResults[i], eventData);
            }
        }
    }

    #endregion

    #region Methods
    
    private void InitHUD()
    {
        foreach (var panel in mainPanels)
        {
            panel.SetActive(false);
        }
        
        playerInfoPanel.SetActive(false);
        gameTitle.SetActive(false);
        
        playerColor = Color.black;
    }

    public void SetActiveNextPanel(GameObject nextPanel)
    {
        if (currentPanel != null)
            currentPanel.SetActive(false);
        nextPanel.SetActive(true);
        currentPanel = nextPanel;

        CheckCurrentPanel();
    }

    private void CheckCurrentPanel()
    {
        if (currentPanel == mainPanels[0])
            gameTitle.SetActive(false);
    }

    public void SetTitleSprite(Sprite titleSprite)
    {
    
        if (gameTitle != null)
        {
            gameTitle.SetActive(true);
            gameTitle.GetComponent<Image>().sprite = titleSprite;
        }
    }

    public static bool IsMouseOverUI()
    {
        return EventSystem.current.IsPointerOverGameObject();
    }

    private void GetColorFromGradient(RaycastResult raycastResult, PointerEventData eventData)
    {
        Color result = raycastResult.gameObject.GetComponent<ColorGradient>().GetPixelColor(eventData.position);
        if (result.a == 1f)
            playerColor = result;
        Debug.LogWarning(playerColor);
    }

    #endregion
}
