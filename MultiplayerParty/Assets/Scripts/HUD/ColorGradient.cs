﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorGradient : MonoBehaviour
{
    public Color GetPixelColor(Vector3 pointerPosition)
    {
        Texture2D gradientTexture = GetComponent<Image>().sprite.texture;
        Vector2 localPointerPosition;
        RectTransform rectTrans = GetComponent<RectTransform>();
        // Camera parameter is null because the Canvas is set to Screen Space - Overlay mode
        RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTrans, pointerPosition, null, out localPointerPosition);

        int localX = Mathf.CeilToInt(localPointerPosition.x + rectTrans.rect.width / 2);
        int localY = Mathf.CeilToInt(localPointerPosition.y + rectTrans.rect.height / 2);

        Color pixelColor = gradientTexture.GetPixel(localX, localY);
        return pixelColor;
    }
}
