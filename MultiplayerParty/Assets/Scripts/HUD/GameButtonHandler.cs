﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameButtonHandler : MonoBehaviour
{
    #region Fields

    [SerializeField]
    private GamesRoomControl.GameOptions.Games game;
    [SerializeField] 
    private Sprite titleSprite;

    private Button button;
    private MenuHUDControl menuHUD;

    #endregion

    #region UnityCallbacks

    private void Start()
    {
        button = GetComponent<Button>();
        menuHUD = FindObjectOfType<MenuHUDControl>();

        if (button != null)
        {
            button.onClick.AddListener(() => menuHUD.SetActiveNextPanel(menuHUD.mainPanels[1]));
            button.onClick.AddListener(() => menuHUD.SetTitleSprite(titleSprite));
            button.onClick.AddListener(() => menuHUD.multiplayerLauncher.GetComponent<GamesRoomControl>().SetChosenGame(game));
        }
    }

    #endregion

    #region Methods



    #endregion
}
