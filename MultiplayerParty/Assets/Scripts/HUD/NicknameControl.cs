﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class NicknameControl : MonoBehaviour
{
    #region Fields

    private TMP_InputField nicknameInput;

    private const string playerNamePrefKey = "PlayerName";
    private string nickname;

    #endregion

    #region UnityCallbacks

    private void Awake()
    {
        if (nicknameInput == null)
            nicknameInput = GetComponent<TMP_InputField>();
        
        nicknameInput.onEndEdit.AddListener(action => SetNickname(nicknameInput.text));
    }

    private void OnEnable()
    {
        string defaultName = string.Empty;
        if (nicknameInput != null)
        {
            if (PlayerPrefs.HasKey(playerNamePrefKey))
                defaultName = PlayerPrefs.GetString(playerNamePrefKey);
            else
                defaultName = playerNamePrefKey;
            
            nicknameInput.text = defaultName;
        }
        
        SetNickname(defaultName);
    }

    #endregion

    #region Methods

    private void SetNickname(string value)
    {
        if (string.IsNullOrEmpty(value))
        {
            Debug.LogWarning("NicknameInput Text is Empty;\nSet previous nickname again.");
            
            nicknameInput.text = nickname;
        }
        else
        {
            nickname = value;
            PhotonNetwork.NickName = value;
            PlayerPrefs.SetString(playerNamePrefKey, value);

            FindObjectOfType<MenuHUDControl>().playerInfoPanel.transform.Find("Nickname").GetComponent<TextMeshProUGUI>().text = value;
        }
        
        
    }

    #endregion
}
