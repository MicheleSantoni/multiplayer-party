﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceRolling : MonoBehaviour
{
    #region Fields

    public float rotationSpeed = 5f;

    #endregion

    #region UnityCallbacks

    private void OnEnable()
    {
        StartCoroutine(RollDice());
    }

    #endregion

    #region Methods

    public IEnumerator RollDice()
    {
        transform.rotation = Quaternion.Euler(Vector3.zero);
        
        while (this.enabled)
        {
            transform.Rotate(new Vector3(transform.rotation.x, transform.rotation.y, transform.rotation.z - rotationSpeed + Time.deltaTime));
            yield return null;
        }
    }

    #endregion
}
