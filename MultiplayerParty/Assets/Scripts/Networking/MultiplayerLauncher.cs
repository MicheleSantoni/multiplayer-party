﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.Networking;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class MultiplayerLauncher : MonoBehaviourPunCallbacks
{
    #region Fields

    public string gameVersion = "1.0.0";
    public RegionControl regionControl;

    private GamesRoomControl gamesRoomControl;
    private bool isConnecting;
    

    #region Events

    public event Action OnGameStarted;
    public event Action OnGameLoaded;
    public event Action OnRoomJoined;

    #endregion

    #endregion

    #region UnityCallbacks

    private void Awake()
    {
        OnGameStarted += Connect;
    }

    private void Start()
    {
        if (regionControl == null)
            regionControl = FindObjectOfType<RegionControl>();
        if (gamesRoomControl == null)
            gamesRoomControl = GetComponent<GamesRoomControl>();
        
        OnGameStarted?.Invoke();
    }

    #endregion

    #region PhotonCallbacks

    public override void OnConnectedToMaster()
    {
        Debug.Log("PUN Basics Tutorial/Launcher: OnConnectedToMaster() was called by PUN");
        
        regionControl.SetCurrentRegion();
        Debug.Log(PhotonNetwork.CloudRegion);
        
        OnGameLoaded?.Invoke();
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        Debug.LogWarningFormat("PUN Basics Tutorial/Launcher: OnDisconnected() was called by PUN with reason {0}", cause);

        isConnecting = false;
        if (PhotonNetwork.InRoom)
            PhotonNetwork.LeaveRoom();
    }
    
    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log("PUN Basics Tutorial/Launcher:OnJoinRandomFailed() was called by PUN. No random room available");
        CreateRoom();
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log("PUN Basics Tutorial/Launcher:OnCreateRoomFailed() was called by PUN. problem on creating a new room");
    }

    public override void OnCreatedRoom()
    {
        Debug.Log("PUN Basics Tutorial/Launcher: OnCreatedRoom() called by PUN. This client created a room.");
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("PUN Basics Tutorial/Launcher: OnJoinedRoom() called by PUN. Now this client is in a room.");
        
        if (PhotonNetwork.IsMasterClient)
        {
            PhotonNetwork.AutomaticallySyncScene = true;
        }
        
        OnRoomJoined?.Invoke();
        
        Debug.Log(PhotonNetwork.IsMasterClient);
        foreach (var player in PhotonNetwork.CurrentRoom.Players)
        {
            Debug.Log(player.Value);
        }
    }

    public override void OnPlayerEnteredRoom(Player other)
    {
        Debug.LogFormat($"OnPlayerEnteredRoom() {other.NickName}");
    }
    
    public override void OnPlayerLeftRoom(Player other)
    {
        Debug.LogFormat($"OnPlayerLeftRoom() {other.NickName}");
    }

    #endregion

    #region Methods

    public void ConnectToSpecificRegion(string region)
    {
        if (PhotonNetwork.IsConnected)
            PhotonNetwork.Disconnect();
        
        PhotonNetwork.ConnectToRegion(region);
    }
    
    public void Connect()
    {
        isConnecting = true;
    
        if (!PhotonNetwork.IsConnected)
        {
            Debug.Log("Is Connecting");
            PhotonNetwork.GameVersion = gameVersion;
            PhotonNetwork.ConnectUsingSettings();
        }
    }
    
    public void Disconnect()
    {
        if (PhotonNetwork.IsConnected && PhotonNetwork.InRoom)
        {
            Debug.Log("Is Disconnecting");
            PhotonNetwork.Disconnect();
        }
    }
    
    public void CreateRoom()
    {
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = (byte)gamesRoomControl.chosenGame.maxPlayers;
        roomOptions.IsVisible = true;
        roomOptions.IsOpen = true;
        Hashtable customRoomProperties = new Hashtable() {{"gm", gamesRoomControl.chosenGame.game}};
        roomOptions.CustomRoomProperties = customRoomProperties;

        PhotonNetwork.CreateRoom(null, roomOptions);
    }

    public void JoinRandomRoom()
    {
        Hashtable expectedCustomRoomProperties = new Hashtable() {{"gm", gamesRoomControl.chosenGame.game}};
        PhotonNetwork.JoinRandomRoom(expectedCustomRoomProperties, (byte)gamesRoomControl.chosenGame.maxPlayers);
    }

    #endregion
}
