﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Photon.Pun;
using Photon.Realtime;
using Sirenix.OdinInspector.Editor;
using UnityEngine;

public class GamesRoomControl : MonoBehaviour
{
    #region Fields

    [Serializable]
    public class GameOptions
    {
        public enum Games
        {
            TIC_TAC_TOE,
            PONG,
            ROCK_PAPER_SCISSOR,
            CONNECT_4,
            TANKS,
            HIDE_N_SEEK
        }
        public Games game;
        public int maxPlayers;
        
        public GameOptions(Games name, int players)
        {
            game = name;
            maxPlayers = players;
        }
    }
    
    [SerializeField]
    public List<GameOptions> games = new List<GameOptions>()
    {
        new GameOptions(GameOptions.Games.TIC_TAC_TOE, 2),
        new GameOptions(GameOptions.Games.PONG, 2),
        new GameOptions(GameOptions.Games.ROCK_PAPER_SCISSOR, 2),
        new GameOptions(GameOptions.Games.CONNECT_4, 2),
        new GameOptions(GameOptions.Games.TANKS, 4)
    };
    
    public GameOptions chosenGame;

    #endregion

    #region UnityCallbacks

    

    #endregion

    #region Methods

    public void SetChosenGame(GameOptions.Games game)
    {
        for (int i = 0; i < games.Count; i++)
        {
            Debug.Log(games[i].game);
            if (games[i].game == game)
            {
                chosenGame = games[i];
                return;
            }
        }
        
        Debug.LogWarning("The choosen game is not available or doesn't exists");
    }
    
    

    #endregion
}
