﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

public class RegionControl : MonoBehaviour
{
    #region Fields

    [Serializable]
    public class Region
    {
        public string token;
        public string name;

        public Region(string tkn, string nm)
        {
            this.token = tkn;
            this.name = nm;
        }

        public static Region GetRegionByToken(string tkn, List<Region> regions)
        {
            Region reg = regions.Find(newRegion => newRegion.token == tkn);
            return reg;
        }
        
        public static Region GetRegionByName(string nm, List<Region> regions)
        {
            Region reg = regions.Find(newRegion => newRegion.name == nm);
            return reg;
        }
    }
    public List<Region> regions = new List<Region>()
    {
        new Region("eu", "Europe"),
        new Region("ru", "Russia"),
        new Region("rue", "Russia East"),
        new Region("us", "USA East"),
        new Region("usw", "USA West"),
    };
    
    public Region currentRegion;
    
    private TMP_Dropdown regionDropdown;

    #region Events

    

    #endregion

    #endregion

    #region UnityCallbacks

    // Start is called before the first frame update
    void Start()
    {
        if (regionDropdown == null)
            regionDropdown = GetComponent<TMP_Dropdown>();

        MultiplayerLauncher multiplayerLauncher = FindObjectOfType<MultiplayerLauncher>();
        regionDropdown.onValueChanged.AddListener(action => multiplayerLauncher.ConnectToSpecificRegion(GetRegionToken()));
        InitRegionDropdown();
    }

    #endregion

    #region PhotonCallbacks

    

    #endregion

    #region Methods

    private void InitRegionDropdown()
    {
        regionDropdown.options.Clear();

        for (int i = 0; i < regions.Count; i++)
        {
            TMP_Dropdown.OptionData option = new TMP_Dropdown.OptionData {text = regions[i].name};
            regionDropdown.options.Add(option);
        }
    }

    public void SetCurrentRegion()
    {
        currentRegion = Region.GetRegionByToken(PhotonNetwork.CloudRegion, regions);
    }

    public void ChangeRegion(string token)
    {
        TMP_Dropdown.OptionData option = new TMP_Dropdown.OptionData {text = token};
        regionDropdown.value = regionDropdown.options.FindIndex(data => data == option);
        regionDropdown.RefreshShownValue();
    }

    private string GetRegionToken()
    {
        string token = String.Empty;
        token = Region.GetRegionByName(regionDropdown.options[regionDropdown.value].text, regions).token;
        return token;
    }

    #endregion
    
    
    
    
}
